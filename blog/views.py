from django.shortcuts import render
from .models import Post
def about(request):
	return render(request, 'blog/about.html')
def index(request):
	posts = Post.objects.all().order_by('published_date')
	return render(request, 'blog/index.html', {'posts':posts})
def contact(request):
	return render(request, 'blog/contact.html')